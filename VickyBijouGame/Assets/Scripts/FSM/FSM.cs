using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FSM : MonoBehaviour
{
    private JugadoresEstadoBase _JEB;
    public void Comenzar(JugadoresEstadoBase p_JEB)
    {
        _JEB = p_JEB;
        _JEB.Inicio();
    }
    public void Actualizar()
    {
        if (this._JEB != null)
        {
            this._JEB.Actualizar();
        }

    }
    public void CambiarEstadoJugador(JugadoresEstadoBase pnuevo_estado)
    {
        _JEB.Terminar();
        _JEB = null;
        _JEB = pnuevo_estado;
        _JEB.Inicio();
    }




}
