using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientesEstadoBase 
{
    public string nombre;
    public FSM m_e;
    public ClientesEstadoBase(string pnombre,FSM pm_e)
    {
        nombre = pnombre;
        m_e = pm_e;
    }
    public virtual void Inicio() { }
    public virtual void Actualizar() { }
    public virtual void Terminar() { }


}
