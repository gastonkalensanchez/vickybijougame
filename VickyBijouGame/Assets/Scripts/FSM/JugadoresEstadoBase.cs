
public interface JugadoresEstadoBase 
{
    public void Inicio();
    public void Actualizar();
    public void Terminar();
}
