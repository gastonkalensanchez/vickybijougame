using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class JugadoresIDLE : JugadoresEstadoBase
{

    private JugadoresController controlador;
    public JugadoresIDLE(JugadoresController pcontroller)
    {
        controlador = pcontroller;
    }


    public void Inicio()
    {
        controlador.tag = "Player";
    }
    public void Actualizar()
    {
        controlador.transform.Translate(1 * Time.deltaTime, 0, 0);
        if (controlador.transform.position.x >= 5f)
        {
            //controlador.CambiarEstadoJugador(controlador._Prueba);
        }
    }

    public void Terminar()
    {
        controlador.tag = "Untagged";
    }
}
