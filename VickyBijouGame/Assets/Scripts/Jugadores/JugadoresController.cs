using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class JugadoresController : FSM
{
    private JugadoresIDLE _IDLE;
    void Awake()
    {
        _IDLE = new JugadoresIDLE(this);
    }

    void Start()
    {
        Comenzar(_IDLE);
    }
    void Update()
    {
        Actualizar();
    }

}
